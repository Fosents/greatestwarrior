public enum Rank {
    Pushover,
    Novice,
    Fighter,
    Warrior,
    Veteran,
    Sage,
    Elite,
    Conqueror,
    Champion,
    Master,
    Greatest
}
