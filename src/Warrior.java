import java.util.ArrayList;

public class Warrior {

    private static final int START_EXP = 100;

    private int experience;

    private ArrayList<String> achievements = new ArrayList<>();

    public Warrior() {
        this.experience = START_EXP;
    }

    public Warrior(int experience) {
        this.experience = experience;
    }

    private void setExperience(int experiencePoints) {
        this.experience += experiencePoints;

        if (this.experience >= 10000) {
            this.experience = 10000;
        }
    }

    public int getExperience() {
        return this.experience;
    }

    public int getLevel() {
        return this.experience /100;
    }

    public Rank getRank() {
        return getRankByLevel(getLevel());
    }

    private Rank getRankByLevel(int level) {
        if (level < 10) {
            return Rank.Pushover;
        } else if (level < 20) {
            return Rank.Novice;
        } else if (level < 30) {
            return Rank.Fighter;
        } else if (level < 40) {
            return Rank.Warrior;
        } else if (level < 50) {
            return Rank.Veteran;
        } else if (level < 60) {
            return Rank.Sage;
        } else if (level < 70) {
            return Rank.Elite;
        } else if (level < 80) {
            return Rank.Conqueror;
        } else if (level < 90) {
            return Rank.Champion;
        } else if (level < 100) {
            return Rank.Master;
        }
        return Rank.Greatest;
    }

    public String Battle(int enemyLevel) {
        if (enemyLevel < 1 || enemyLevel > 100) {
            return "Invalid enemy level";
        }

        if (enemyLevel == this.getLevel()) {
            this.setExperience(10);
        } else if (this.getLevel() == enemyLevel + 1) {
            this.setExperience(5);
        } else if (this.getLevel() >= enemyLevel + 2) {
            this.setExperience(0);
            return "Easy fight";
        } else if (this.getLevel() < enemyLevel) {
            if (
                    this.getRank().ordinal() < this.getRankByLevel(enemyLevel).ordinal() &&
                    this.getLevel() <= enemyLevel - 5
            ) {
                return "You've been defeated";
            }
            int diff = enemyLevel - this.getLevel();
            this.setExperience(diff * diff * 20);
        }
        return "An intense fight";
    }

    public String Training(String description, int trainingExperience, int levelRequirement) {
        if (this.getLevel() < levelRequirement) {
            return "Not strong enough";
        }
        this.setExperience(trainingExperience);
        this.achievements.add(description);
        return description;
    }
}