import java.util.Scanner;

public class Battle {

    public static void main(String[] args) {

        Warrior kotrag = new Warrior();

        Scanner scanner = new Scanner(System.in);

        while (true) {

            if (kotrag.getExperience() == 10000) {
                System.out.println("Congratulations! You've reached max level!");
                break;
            }

            System.out.println("ENTER ENEMY LEVEL");
            int enemyLevel = scanner.nextInt();

            if (kotrag.Battle(enemyLevel).equals("You've been defeated")) {
                System.out.println(kotrag.Battle(enemyLevel) + "\nStart Over");
                break;
            } else {

                System.out.println(kotrag.Battle(enemyLevel));
                System.out.println("You've reached level " + kotrag.getLevel());
                System.out.println("Rank: " + kotrag.getRank());
                System.out.println("Experience: " + kotrag.getExperience() + "\n");
            }
        }
    }
}