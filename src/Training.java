public class Training {
    public static void main(String[] args) {
        Warrior byorn = new Warrior(2000);

        System.out.println((byorn.Training("BRAVO", 30, 10)));
        System.out.println("You've reached level " + byorn.getLevel());
        System.out.println("Rank: " + byorn.getRank());
        System.out.println("Experience: " + byorn.getExperience());
    }
}